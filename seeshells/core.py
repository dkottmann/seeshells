__author__ = '''Dan Kottmann <djkottmann@gmail.com>
                Chris Patten <cpatten@packetresearch.com>'''


import util
import msgpack
import re
import urllib2
import time
import smtplib


class MetasploitRPC():

    def __init__(self, host='127.0.0.1', port=55552, user='msf', password=''):
        # Setup base values for MSF RPC
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.auth_token = self._login()
        self.cid = self._create_console()

    # Builds a basic request used for RPC requests
    def _get_vanilla_request(self):
        base_url = "http://" + self.host + ":" + str(self.port) + "/api/"
        base_request = urllib2.Request(base_url)
        base_request.add_header('Content-type', 'binary/message-pack')
        return base_request

    # Attempts to login via MSF's RPC interface
    def _login(self):
        options = ['auth.login', self.user, self.password]
        # Initiate RPC call
        response = self._run(params=options, auth=False, console=False)

        token = None
        if response.get('result') == 'success':
            print "[+] Authentication successful"
            token = response.get('token')
        else:
            util.croak("Authentication failed")

        # Return session token
        return token

    # Certain MSF commands require a console. This creates a console.
    def _create_console(self):
        options = ['console.create']
        response = self._run(params=options, console=False)
        if response.get('id') is None:
            util.croak("Unable to create console")
        print "[+] Console {0} created".format(response.get('id'))
        cid = response.get('id')
        self.cid = cid
        response = self._run(params=['console.read'])
        # Return console Id
        return cid

    # Function that actually sends RPC request
    def _run(self, params=None, auth=True, console=True):
        if not params:
            # Build default params
            params = list()

        # Check if auth is required and was successful
        if auth and not self.auth_token:
            util.croak("You must first log in to MSF")

        # Check if console required and was created
        if console and not self.cid:
            util.croak("Console does not exist")

        # Insert session token if required
        if auth:
            params.insert(1, self.auth_token)

        # Set console id if needed
        if console:
            params.insert(2, self.cid)

        # Build request with params
        request = self._get_vanilla_request()
        query_params = msgpack.packb(params)
        request.add_data(query_params)

        # Send RPC request
        response = msgpack.unpackb(urllib2.urlopen(request).read())
        if params[0] == 'console.write':
            time.sleep(1)
            while True:
                # Read the response until there's no more data
                response = self._run(params=['console.read'])

                if response.get('busy', False):
                    time.sleep(1)
                    continue

                break

        return response

    # Retrieve list of Meterpreter sessions via RPC
    def get_sessions(self):
        ret = self._run(params=['session.list'], console=False)
        return ret

    # Execute a powershell script against a session
    def exec_powershell(self, ps1, session_id):
        cmd = """
            use post/windows/manage/powershell/exec_powershell
            set SCRIPT {0}
            set SESSION {1}
            exploit
        """.format(ps1, session_id)
        ret = self._run(params=['console.write', cmd])
        return ret


class Alerter():

    def __init__(self, host, port, smsdict):
        # Init variables for smtp server and alerts
        self.host = host
        self.port = port
        self.server = None
        self._connect(host, port)
        self.smsto = smsdict['to']
        self.smsfrom = smsdict['from']

    def _connect(self, host, port):
        # Connect to SMTP server
        self.server = smtplib.SMTP(host, port)

    def send_sms(self, message):
        for sms_addy in self.smsto:
            # Send emails to each address
            try:           
                self.server.sendmail(self.smsfrom, sms_addy, message)
            except smtplib.SMTPServerDisconnected:
                print "[!] Reconnecting to SMTP..."
                self._connect(self.host, self.port)
                self.send_sms(message)


class Portmon():

    # Parses the port variables
    def portextract(self, portentry):
        if ":" in portentry:
            # Port is in form host:port
            mon_host, mon_port = portentry.split(':')
            if int(mon_port) < 1 or int(mon_port) > 65535:
                util.croak("Invalid port number")
            else:
                return 'netstat -an | grep "%s.%s " | grep LISTEN | ' \
                    'grep -v grep' % (mon_host, mon_port), mon_host, mon_port
        else:
            # Port doesn't include host
            mon_port = portentry
            if int(mon_port) < 1 or int(mon_port) > 65535:
                util.croak("Invalid port number")
            else:
                return 'netstat -an | egrep "(127\.0\.0\.1|\*|0\.0\.0\.0)[\.:]%s " | grep LISTEN | ' \
                    'grep -v grep' % mon_port, None, mon_port

    # Parses multiple ports from a port file
    def portmultimon(self, filename):
        ipportlist = []
        with open(filename, "r") as pf:
            for line in pf:
                if ":" in line:
                    # Port is in form host:port
                    hplist = line.split(":")
                    ipportlist.append(hplist[0] + ":" + hplist[1].strip())
                else:
                    # Port doesn't include host
                    port = line.strip()
                    ipportlist.append(port)
        return ipportlist


class Filtermon():

    def __init__(self, su, sd, si):
        self.su = su
        self.sd = sd
        self.si = si

    # Checks if a new Meterpreter session contains any
    # of the users configured in filter
    def evaluser(self, sid):
        suser = None
        sidcapgrp = re.search(r'([ A-Za-z0-9]+)\\([A-Za-z0-9]+\b)', sid)
        if sidcapgrp:
            user = sidcapgrp.group(2).lower()
            for s in self.su:
                s = s.lower()
                if s == user:
                    suser = s

        return suser

    # Checks if a new Meterpreter session contains any
    # of the domains configured in filter
    def evaldomain(self, sid):
        sdom = None
        sidcapgrp = re.search(r'(.+)?\\(.+\b)', sid)
        if sidcapgrp:
            domain = sidcapgrp.group(1).lower()
            for s in self.sd:
                if s.lower() == domain:
                    sdom = s

        return sdom

    # Checks if a new Meterpreter session contains any
    # of the IP addresses configured in filter
    def evalip(self, sid):
        sip = None
        for s in self.si:
            if s == sid:
                sip = s

        return sip
